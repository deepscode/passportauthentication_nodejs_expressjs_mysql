import express from 'express';
import passport from 'passport';
import bodyParser from 'body-parser';
import PassportConfig from './configure/passport';
import {MySQlWrapper}  from "./db";
import session from 'express-session';
import uuid from 'uuid/v4';
import {} from 'dotenv/config';
import {UserDbHelper} from './db';

let app = express();


let mySqlWrapper = new MySQlWrapper();
let passportConfig = new PassportConfig(mySqlWrapper);
let userHelper = new UserDbHelper(mySqlWrapper); 

app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json());
var strategy = passportConfig.SetStaregy();

app.use(session({
    secret:process.env.secret,
    saveUninitialized:true,
    resave:false,
    cookie : { httpOnly: true, maxAge: 2419200000 } // configure when sessions expires
}));


app.get('/',function(req,res){
     res.send('node API is up and running.');
});

app.post('/login',strategy.authenticate('local',{session:true}),(req,res)=>{
    //console.log(req.user);
    if(req.user != undefined || req.user != null){
        res.send(req.user);
    }else{
        res.send("not able to generate the token.username or password incorrect.");
    }
});


app.post('/register',(req,res)=>{
    console.log(req.body);
    if(req.body !== undefined && req.body !== null){
        userHelper.registerUser(req.body.username, req.body.password).then(()=>{
            res.send("user registered successfully.");
        }).catch(ex=>{
            console.log(ex);
            res.send("not able to generate the token.username or password incorrect.",ex);
        })
    }
});



app.get('/todos',strategy.authenticate('bearer',{session:true}),(req,res)=>{
    res.send({toods:'check'});
});

app.listen(process.env.port,()=>{
    console.log(`server is running on port : ${process.env.port}`);
});